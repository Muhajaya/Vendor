OrangeFox Translators
---------------------

Chinese Simplified:
- qetxdc
German:
- Fabian
Greek:
- spkprs
Hungarian:
- MKAdam
Italian:
- Filippo Giovannini
Indonesian:
- Lazuardi Elki
- Ninja_14
- muhajaya
Polish:
- makstrix
Romanian:
- Igor Sorocean
Russian:
- MrYacha
- fordownloads
Spanish:
- Jimgsey
Turkish:
- yanikyigit59
- Metin Arslan